﻿using HoldfastGame;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Unity;
using UnityEngine;

namespace uMod.Game.Holdfast
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class HoldfastPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        internal RoundPlayer roundPlayer;
        internal RoundPlayerInformation roundPlayerInfo;

        public HoldfastPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
        }

        public HoldfastPlayer(RoundPlayer roundPlayer) : this(roundPlayer.PlayerRoundInformation.SteamID.ToString(), roundPlayer.PlayerRoundInformation.InitialDetails.Name)
        {
            // Store player object
            this.roundPlayer = roundPlayer;
            roundPlayerInfo = roundPlayer.PlayerRoundInformation;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets/sets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            roundPlayer = gamePlayer as RoundPlayer;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the player's IP address
        /// </summary>
        public string Address
        {
            get
            {
                /*P2PSessionState_t sessionState;
                SteamGameServerNetworking.GetP2PSessionState(cSteamId, out sessionState);
                uint ip = sessionState.m_nRemoteIP;
                return string.Concat(ip >> 24 & 255, ".", ip >> 16 & 255, ".", ip >> 8 & 255, ".", ip & 255);*/
                throw new NotImplementedException(); // TODO: Implement if possible
            }
        }

        /// <summary>
        /// Gets the player's average network ping
        /// </summary>
        public int Ping => roundPlayer.NetworkPlayer.averagePing;

        /// <summary>
        /// Gets the player's language
        /// </summary>
        public CultureInfo Language => CultureInfo.GetCultureInfo("en"); // TODO: Implement when possible

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Administrators); // TODO: Implement if possible

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin || BelongsToGroup(Interface.uMod.Auth.Configuration.Groups.Moderators); // TODO: Implement if possible

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => throw new NotImplementedException(); // TODO: Implement if possible

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => roundPlayer.NetworkPlayer.isConnected;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => throw new NotImplementedException(); // TODO: Implement if possible

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping => throw new NotImplementedException(); // TODO: Implement if possible

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player
                // TODO: Ban the player
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero; // TODO: Implement when possible

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason)
        {
            if (IsConnected)
            {
                // TODO: Kick player
                throw new NotImplementedException(); // TODO: Implement if possible
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                throw new NotImplementedException(); // TODO: Implement if possible
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => throw new NotImplementedException(); // TODO: Implement if possible
            set => throw new NotImplementedException(); // TODO: Implement if possible
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                return 1000f; // TODO: Implement if possible
            }
            set
            {
                throw new NotImplementedException(); // TODO: Implement if possible
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (Health > 0f && !IsDead)
            {
                Health = Mathf.Min(Health + amount, MaxHealth);
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount) => Health = amount; // TODO: Check for better method

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => Hurt(1000f);

        /// <summary>
        /// Renames the player to specified name
        /// <param name="name"></param>
        /// </summary>
        public void Rename(string name) => roundPlayerInfo.InitialDetails.Name = name;

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement if possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement if possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement if possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position() => roundPlayer.PlayerTransform.position.ToPosition();

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            Vector3 pos = new Vector3(x, y, z);
            roundPlayer.PlayerBase.Teleport(pos);
            uLinkNetworkConnectionsCollection.LocalNetworkView.RPC("TeleportPlayer", uLink.RPCMode.Others, roundPlayer.NetworkPlayerID, pos);
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement if possible
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement if possible
        }

        #endregion Chat and Commands
    }
}

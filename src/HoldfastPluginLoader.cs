﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Holdfast
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class HoldfastPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Holdfast) };

        public HoldfastPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}

﻿using uMod.IO;

namespace uMod.Game.Holdfast
{
    public class HoldfastConfiguration : TomlFile
    {
        /// <summary>
        /// Create a new instance of a server configuration
        /// </summary>
        /// <param name="filename"></param>
        public HoldfastConfiguration(string filename) : base(filename)
        {
        }
    }
}

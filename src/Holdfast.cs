﻿using HoldfastGame;
using Steamworks;
using uMod.Plugins;
using uMod.Plugins.Decorators;

namespace uMod.Game.Holdfast
{
    /// <summary>
    /// The core Holdfast: Nations at War plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Holdfast : Plugin
    {
        #region Initialization

        internal static readonly HoldfastProvider Universal = HoldfastProvider.Instance;

        // Game instances
        internal static ServerAdminBroadcastMessageManager messageManager;
        internal static ServerBannedPlayersManager banManager;
        internal static ServerConfigurationFile serverConfig;
        internal static ServerConnectionManager connectionManager;
        internal static GameDetails gameDetails;
        internal static ServerGameManager gameManager;

        private bool serverInitialized;

        /// <summary
        /// <summary>
        /// Initializes a new instance of the Holdfast class
        /// </summary>
        public Holdfast()
        {
            // Set plugin info attributes
            Title = "Holdfast: Nations at War";
            Author = HoldfastExtension.AssemblyAuthors;
            Version = HoldfastExtension.AssemblyVersion;
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();

            serverInitialized = true;

            SteamGameServer.SetGameTags("modded,umod");

            messageManager = ServerComponentReferenceManager.ServerInstance.serverAdminBroadcastMessageManager;
            banManager = ServerComponentReferenceManager.ServerInstance.serverBannedPlayersManager;
            serverConfig = ServerConfigurationFileManager.CurrentConfigurationFile;
            connectionManager = ServerComponentReferenceManager.ServerInstance.serverConnectionManager;
            gameDetails = ServerComponentReferenceManager.ServerInstance.serverGameManager.CreateGameDetailsFromConfigFile(0);
            gameManager = ServerComponentReferenceManager.ServerInstance.serverGameManager;
        }

        #endregion Initialization
    }
}

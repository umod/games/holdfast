﻿using HoldfastGame;
using uMod.Common;
using uMod.Common.Command;
using uMod.Text;
using uMod.Utilities;

namespace uMod.Game.Holdfast
{
    /// <summary>
    /// Provides Universal functionality for the game server
    /// </summary>
    public class HoldfastProvider : IUniversalProvider
    {
        /// <summary>
        /// Gets the name of the game for which this provider provides
        /// </summary>
        public string GameName => "Holdfast: Nations at War";

        /// <summary>
        /// Gets the Steam app ID of the game's client, if available
        /// </summary>
        public uint ClientAppId => 589290;

        /// <summary>
        /// Gets the Steam app ID of the game's server, if available
        /// </summary>
        public uint ServerAppId => 589290;

        /// <summary>
        /// Gets a container with important game-specific types
        /// </summary>
        public IGameTypes Types { get; private set; }

        /// <summary>
        /// Gets the singleton instance of this provider
        /// </summary>
        internal static HoldfastProvider Instance { get; private set; }

        public HoldfastProvider()
        {
            Instance = this;
        }

        /// <summary>
        /// Gets the command system provider
        /// </summary>
        public HoldfastCommands CommandSystem { get; private set; }

        /// <summary>
        /// Creates the game-specific command system provider object
        /// </summary>
        /// <param name="commandHandler"></param>
        /// <returns></returns>
        public ICommandSystem CreateCommandSystemProvider(ICommandHandler commandHandler) => CommandSystem = new HoldfastCommands(commandHandler);

        /// <summary>
        /// Creates the game specific player manager
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public IPlayerManager CreatePlayerManager(IApplication application, ILogger logger)
        {
            return new HoldfastPlayerManager(application, logger);
        }

        /// <summary>
        /// Creates the game-specific server object
        /// </summary>
        /// <returns></returns>
        public IServer CreateServer()
        {
            IServer server = new HoldfastServer();
            Types = new GameTypes(server)
            {
                Player = typeof(RoundPlayer)
            };

            return server;
        }

        /// <summary>
        /// Formats the text with universal markup as game-specific markup
        /// </summary>
        /// <param name="text">text to format</param>
        /// <returns>formatted text</returns>
        public string FormatText(string text) => Formatter.ToPlaintext(text);
    }
}

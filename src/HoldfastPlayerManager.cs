﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Holdfast
{
    /// <summary>
    /// Represents a Holdfast player manager
    /// </summary>
    public class HoldfastPlayerManager : PlayerManager<HoldfastPlayer>
    {
        /// <summary>
        /// Create a new instance of the HoldfastPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public HoldfastPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}
